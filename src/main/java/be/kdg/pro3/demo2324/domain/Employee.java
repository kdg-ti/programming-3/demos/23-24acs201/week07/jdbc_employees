package be.kdg.pro3.demo2324.domain;

import java.time.LocalDate;

public class Employee {
	int id;
	String name;
	int salary;
	LocalDate birthday;

	public Employee(int id, String name, int salary, LocalDate birthday) {
		this.id = id;
		this.name = name;
		this.salary = salary;
		this.birthday = birthday;
	}

	public Employee(String name, int salary, LocalDate birthday) {
		this.name = name;
		this.salary = salary;
		this.birthday = birthday;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	public LocalDate getBirthday() {
		return birthday;
	}

	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}

	@Override
	public String toString() {
		return String.format("%2d %30s %6d %10s%n",id,name ,salary ,birthday );
	}
}
