package be.kdg.pro3.demo2324;

import be.kdg.pro3.demo2324.domain.Employee;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import be.kdg.pro3.demo2324.repository.EmployeeRepository;

import java.time.LocalDate;

@Component
public class JdbcRunner implements CommandLineRunner {
	private static Logger log = LoggerFactory.getLogger(JdbcRunner.class);

	private EmployeeRepository employeeRepository;

	public JdbcRunner(EmployeeRepository employeeRepository) {
		this.employeeRepository = employeeRepository;
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println(employeeRepository.findAll());
		System.out.println(employeeRepository.insert(new Employee("Lars",3800,LocalDate.of(1995,6,3))));

	}
}
