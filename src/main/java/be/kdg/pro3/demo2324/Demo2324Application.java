package be.kdg.pro3.demo2324;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demo2324Application {

	public static void main(String[] args) {
		SpringApplication.run(Demo2324Application.class, args);
	}

}
