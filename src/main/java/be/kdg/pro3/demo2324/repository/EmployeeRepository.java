package be.kdg.pro3.demo2324.repository;

import be.kdg.pro3.demo2324.domain.Employee;

import java.util.List;

public interface EmployeeRepository {
	Employee insert(Employee employee);
	List<Employee> findAll();
}
