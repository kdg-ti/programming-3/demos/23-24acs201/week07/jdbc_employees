package be.kdg.pro3.demo2324.repository;

import be.kdg.pro3.demo2324.domain.Employee;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

//@Repository
public class EmployeeJdbcRepository implements EmployeeRepository {
	private static Logger log = LoggerFactory.getLogger(EmployeeJdbcRepository.class);
	String url;

	public EmployeeJdbcRepository(@Value ("${spring.datasource.url}") String url) {
		this.url = url;
	}

	@Override
	public Employee insert(Employee employee) {
		try (Connection cx = DriverManager.getConnection(url);
		     PreparedStatement insertStatement = cx.prepareStatement("""
			         INSERT INTO EMPLOYEES(NAME,SALARY,BIRTHDAY)
			     VALUES (?,?,?)
			     	""",Statement.RETURN_GENERATED_KEYS)) {
			insertStatement.setString(1,employee.getName() );
			insertStatement.setInt(2,employee.getSalary() );
			insertStatement.setDate(3,Date.valueOf(employee.getBirthday() ));
			insertStatement.executeUpdate();
			try(ResultSet keySet = insertStatement.getGeneratedKeys()){
				if(keySet.next()){
					employee.setId(keySet.getInt(1));
				}
				return employee;
			}
		} catch (SQLException e) {
			log.error("error inserting employee");
			return null;
		}
	}

	@Override
	public List<Employee> findAll() {
		List<Employee> employees = new ArrayList<>();
		try (Connection cx = DriverManager.getConnection(url);
		     Statement statement = cx.createStatement();
		     ResultSet result = statement.executeQuery("Select * from employees")) {
			while (result.next()) {
//				System.out.printf("%2d %30s %6d %10s%n",
//					result.getInt("ID") ,
//						result.getString("NAME") ,
//						result.getInt("SALARY") ,
//					result.getDate("BIRTHDAY"));
				employees.add(new Employee(result.getInt("ID"),
					result.getString("NAME"),
					result.getInt("SALARY"),
					result.getDate("BIRTHDAY").toLocalDate()));
			}	} catch (SQLException e) {
			log.error("error inserting employee");
		}
		return employees;
	}
	}

