package be.kdg.pro3.demo2324.repository;

import be.kdg.pro3.demo2324.domain.Employee;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.*;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class EmployeeJdbcTemplateRepository implements EmployeeRepository {
	private static Logger log = LoggerFactory.getLogger(EmployeeJdbcTemplateRepository.class);
	private JdbcTemplate jdbcTemplate;

	public EmployeeJdbcTemplateRepository(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public Employee insert(Employee employee) {
		try (Connection cx = DriverManager.getConnection(url);
		     PreparedStatement insertStatement = cx.prepareStatement("""
			         INSERT INTO EMPLOYEES(NAME,SALARY,BIRTHDAY)
			     VALUES (?,?,?)
			     	""", Statement.RETURN_GENERATED_KEYS)) {
			insertStatement.setString(1, employee.getName());
			insertStatement.setInt(2, employee.getSalary());
			insertStatement.setDate(3, Date.valueOf(employee.getBirthday()));
			insertStatement.executeUpdate();
			try (ResultSet keySet = insertStatement.getGeneratedKeys()) {
				if (keySet.next()) {
					employee.setId(keySet.getInt(1));
				}
				return employee;
			}
		} catch (SQLException e) {
			log.error("error inserting employee");
			return null;
		}
	}


	public List<Employee> findWithHandler() {
		List<Employee> employees = new ArrayList<>();
		 jdbcTemplate.query(
			"Select * from employees",
			(RowCallbackHandler) result ->
				employees.add(new Employee(result.getInt("ID"),
			result.getString("NAME"),
			result.getInt("SALARY"),
			result.getDate("BIRTHDAY").toLocalDate()))
		);
		return employees;
	}


	@Override
	public List<Employee> findAll() {
		return jdbcTemplate.query("Select * from employees", (result, nr) -> EmployeeRowMapper(result, nr));
	}

	private Employee EmployeeRowMapper(ResultSet result,int nr) throws SQLException {
		return  new Employee(result.getInt("ID"),
			result.getString("NAME"),
			result.getInt("SALARY"),
			result.getDate("BIRTHDAY").toLocalDate());
	}


}

